fs = require('fs')
compiler = require('./compiler')
args = process.argv
args.shift()
args.shift()
srcPath = args.shift()
if !srcPath
  throw new Error('No source provided')
src = fs.readFileSync(srcPath).toString()
compiled = compiler.compile(src)
destPath = args.shift()
console.log ('Compiled succesfully')
if destPath
  fs.writeFileSync destPath, compiled, 'utf8'
  console.log 'written to ' + destPath