allowedTags = ['div', 'span', 'input', 'h1', 'h2', 'button', 'small', 'footer', 'nav',
  'a', 'canvas', 'caption', 'center', 'div', 'fieldset',
    'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'i', 'img', 'label', 'legend',
    'li', 'ol', 'p', 'span', 'strong', 'sub', 'sup', 'table', 'br',
    'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'ul', 'style']

checkTagPrefix = (candidate, options)->
  return false if not options?.tagPrefixes?
  for prefix in options.tagPrefixes
    return true if candidate.indexOf(prefix) is 0
  return false

parseLine = (lineContent, options)->
  if lineContent[0] is '.'
    return typedLine =
      type: 'jade'
      content: lineContent
  if lineContent[0] is '~'
    return typedLine =
      type: 'jade'
      content: lineContent.substr(1)
  if lineContent[0] is '#'
    return typedLine =
      type: 'comment'
      content: lineContent
  if lineContent[0] is '|'
    return typedLine =
      type: 'plain'
      content: '"' + lineContent.substr(1) + '"'
  if lineContent[0] is '%'
    return typedLine =
      type: 'code'
      content: lineContent.substr(1)

  checkJade = lineContent.match(/^([a-zA-Z0-9-_]*)/)
  if checkJade
    #log lineContent[checkJade[1].length]

    if not lineContent[checkJade[1].length]? or lineContent[checkJade[1].length] isnt ':'
      #log 'JADE         ' + lineContent
      if allowedTags.indexOf(checkJade[1]) > -1 or checkTagPrefix(checkJade[1], options)
        return typedLine =
          type: 'jade'
          content: lineContent
    #log 'NOT JADE         ' + lineContent
  if /^([a-zA-Z0-9_\'']*)( )*:/.test(lineContent)
    return typedLine =
      type: 'hash'
      content: lineContent
  return typedLine =
      type: 'code'
      content: lineContent

module.exports = (src, options)->
  #Удаляем символ возврата каретки, если он есть, нам достаточно new line
  src = src.replace(/\r/g, '')
  #Начинаем парсить построчно
  str = src.split('\n')
  result = []
  for line in str
    parsed = line.match(/^( *)(.+)$/)
    if parsed
      indentationSpaces = parsed[1] #наденные trailing spaces
      lineContent = parsed[2] #содержимое строки
      if lineContent.length > 0
          typedLine = parseLine(lineContent, options)
      else
          typedLine =
            type: 'blank'
            content: ''
      typedLine.indented = indentationSpaces.length
    else
      typedLine =
        type: 'blank'
        content: ''
        indented: 0


    result.push typedLine

  return result
