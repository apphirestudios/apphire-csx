if not global?.log?
  global.log = console.log.bind @
parse = require './parse'
ast = require './ast'
make = require './make'

compile = (src, options) ->
  parsedLines = parse(src, options)
  #log JSON.stringify(parsedLines, null, 2)
  tree = ast(parsedLines, options)
  #log JSON.stringify(ast.code, null, 2)
  #log JSON.stringify(ast.vdoms, null, 2)
  compiled = make(tree, options)
  #log compiled
  return compiled



module.exports =
  compile: compile
