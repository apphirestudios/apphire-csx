vdomsEnum = 0
makeIndentation = (count)->
  str = ''
  i = 1
  while i <= count
    str = str + ' '
    i++
  return str

pos = 0
lines = undefined

buildNode = (isRoot)->
    line = lines[pos]
    node = {}
    switch line.type
      when 'jade'
        node.type = 'jade'
        node.tag = line.content.match(/^([a-zA-Z0-9-_\.\#]*)/)[1]
        node.sub = []
        if plainStrParsed = line.content.match(/\|(.*)$/)
          node.sub.push
            type: 'plain'
            plainStr: '"' + plainStrParsed[1] + '"'
        if inlineHashesParsed = line.content.match(/\((.*)\)/)
          hashes = inlineHashesParsed[1].split(',')
          for hash in hashes
            node.sub.push
              type: 'hash'
              hashStr: hash.replace('=', ': ')

        if lines[pos + 1]?.indented > line.indented or lines[pos + 1]?.type is 'blank' or lines[pos + 1]?.type is 'comment'
          pos = pos + 1
          while lines[pos]?.indented > line.indented or lines[pos]?.type is 'blank' or lines[pos]?.type is 'comment'
            node.sub.push buildNode()
        else
          pos = pos + 1

      when 'hash'
        node.type = 'hash'
        node.hashStr = line.content
        pos = pos + 1
        while lines[pos]?.indented > line.indented or lines[pos]?.type is 'blank' or lines[pos]?.type is 'comment'
          node.hashStr = node.hashStr + '\n' + makeIndentation(lines[pos].indented - line.indented) + lines[pos].content
          pos = pos + 1
        ###
        while (lines[pos]?.indented >= line.indented and lines[pos].type isnt 'jade') and (lines[pos].type isnt 'code' or lines[pos].indented > line.indented)
          node.hashStr = node.hashStr + '\n' + makeIndentation(lines[pos].indented - line.indented) + lines[pos].content
          pos = pos + 1
        ###
      when 'code'
        node.type = 'func'
        node.funcStr = line.content
        node.sub = []
        pos = pos + 1
        #TODO если нужно, чтобы работали не только функции, но и выражения и всякие switch, то менять на >= первое неравенство
        getStrategy = ->
          if isRoot?
            return lines[pos]?.indented >= line.indented
          else
            return lines[pos]?.indented >= line.indented
        while getStrategy() or lines[pos]?.type is 'blank' or lines[pos]?.type is 'comment'
          if lines[pos].type is 'jade'
            if lines[pos]?.indented is line.indented then break

            indentDif = makeIndentation(lines[pos].indented - line.indented)
            node.funcStr = node.funcStr + '\n'
            node.funcStr = node.funcStr + indentDif + 'vdomMarker' + vdomsEnum + '\n'
            vdomsEnum = vdomsEnum + 1
            node.sub.push buildNode()
          else
            node.funcStr = node.funcStr + '\n' + makeIndentation(lines[pos].indented - line.indented) + lines[pos].content
            pos = pos + 1

      when 'blank'
        node.type = 'blank'
        pos = pos + 1

      when 'comment'
        node.type = 'comment'
        node.commentStr = lines[pos].content
        pos = pos + 1
      when 'plain'
        node.type = 'plain'
        node.plainStr = lines[pos].content
        pos = pos + 1
      else
        throw new Error 'Unexpected error happened with node ' + node
    return node

module.exports = (typedLines)->
  pos = 0
  lines = typedLines
  vdomsEnum = 0
  while pos < typedLines.length - 1
    if typedLines[pos].type is 'code'
      return buildNode(isRoot = true)
    pos = pos + 1
  return undefined


