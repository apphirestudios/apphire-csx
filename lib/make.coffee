hasVdoms = false
vdoms = []
mode = 'development'
prepareNode = (node, startIndentation)->

  str = node.funcStr
  if node.sub?.length > 0
    str = str.replace /( *)(vdomMarker)([0-9]*)/g, (str, spaces, vdomMarker, markerNumber)->
      if mode is 'development'
        replacer =  spaces + "__construct(eval('new Object(' + __serialize(VDOMS[" + markerNumber + "]) + ')'), VDOMS[" + markerNumber + "])\n"
        vdoms.push('VDOMS[' + markerNumber + '] =\n' + prepareLeaf(node.sub.shift(), '', true))
      else
        replacer =  spaces + '__construct\n' + prepareLeaf(node.sub.shift(), spaces, true)
      return replacer
  return str

prepareLeaf = (node, indent, noMainBraces)->
  str = ''
  if not noMainBraces? then  str = indent + "{\n"
  switch node.type
    when 'jade'
      hasVdoms = true
      str = str + indent+ "  type: 'jade'\n"
      str = str + indent + "  tag: '" + node.tag + "'\n"
      if node.sub?.length > 0
        subStr = ''
        for children in node.sub
          subStr = subStr + prepareLeaf(children, indent + '    ') + '\n'
        str = str + indent + '  sub: [\n' + subStr + indent + '  ]\n'
    when 'hash'
      str = str + '  ' + indent + "type: 'hash'\n"
      str = str + '  ' + indent + "hashStr: " + JSON.stringify(node.hashStr) + '\n'
      str = str + '  ' + indent + 'hash: ()->\n'
      str = str + node.hashStr.replace(/^/gm, indent + '    ') + '\n'

    when 'func'
      str = str + '  ' + indent + "type: 'func'\n"
      str = str + '  ' + indent + 'func: ()->\n'
      str = str + node.funcStr.replace(/^/gm, indent + '    ') + '\n'
      if node.sub?.length > 0
        str = str.replace /( *)(vdomMarker)([0-9]*)/g, (str, spaces, vdomMarker, markerNumber)->
          if mode is 'development'
            replacer =  spaces + "__construct(eval('new Object(' + __serialize(VDOMS[" + markerNumber + "]) + ')'), VDOMS[" + markerNumber + "])\n"
            vdoms.push('VDOMS[' + markerNumber + '] =\n' + prepareLeaf(node.sub.shift(), '', true))
          else
            replacer =  spaces + '__construct\n' + prepareLeaf(node.sub.shift(), spaces, true)
          return replacer
      str = str + '  ' + indent + "funcStr: " + JSON.stringify(node.funcStr) + '\n'


    when 'plain'
      str = str + '  ' + indent + "type: 'plain'\n"
      str = str + '  ' + indent + "plainStr: " + JSON.stringify(node.plainStr) + '\n'
      str = str + '  ' + indent + "plain: " + node.plainStr + '\n'

    when 'comment'
      str = str + '  ' + indent + "type: 'comment'\n"
      str = str + '  ' + indent + "commentStr: " + JSON.stringify(node.commentStr) + '\n'
      str = str + '  ' + indent + "comment: '" + node.commentStr + "'" + '\n'

    when 'blank'
      str = str + '  ' + indent + "type: 'blank'\n"


  if not noMainBraces? then str = str + indent + '}'

  return str

module.exports =
  makeModule = (ast, options)->
    if options and options.mode isnt 'development' then mode = 'production'
    vdoms = []
    hasVdoms = false
    if not ast? then return ''
    compiledCodePart = ''
    nodes = prepareNode(ast)
    if hasVdoms
      compiledCodePart = compiledCodePart + '__construct = require("apphire-view").construct\n'
      if mode is 'development'
        compiledCodePart = compiledCodePart + '__serialize = require("apphire-view").serializeObject\n'
        compiledCodePart = compiledCodePart + 'window.__views ?= {}\n'
        compiledCodePart = compiledCodePart + 'VDOMS=[]\n'
        compiledCodePart = compiledCodePart + 'window.__views["' + options.filename + '"] =\n  code:' + JSON.stringify(ast.funcStr) + '\n  vdoms: VDOMS\n'
      if mode is 'development'
        vdomsStr = ''
        for vdom in vdoms
          vdomsStr = vdomsStr + vdom
    compiledCodePart = compiledCodePart +  nodes + '\n'
    if mode is 'development'
      compiledCodePart = compiledCodePart + vdomsStr + '\n'
    resultedModule = compiledCodePart + '\n'

    return resultedModule